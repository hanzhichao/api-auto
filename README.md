# ApiAuto
悟空crm接口测试框架，基于Pytest

## 特性
- **基础**
    - 测试报告: allure-pytest
    - 发邮件：自己封装发邮件函数+钩子函数+zip压缩
- **易维护**
    - 日志：有日志功能，日志中有请求及响应信息: Pytest配置+logging+封装请求方法
- **灵活性**
    - 标记和等级：自定义pytest.mark各种标记，pytest-level
    - 多环境切换：pytest-base-url
- **稳定性**
    - 超时时间：pytest-timeout
    - 失败用例自动重跑 pytest-rerunfailures
- **易用**
    - 要有使用说明
    - 封装丰富的工具（Fixture）
    - 用例编写尽可能简单
- **效率**
    - 编写效率：数据分离+数据驱动处理等价类类型的用例
    - 运行效率：并行pytest-xdist（启动多个进程执行用例）

> conftest.py的三重作用
1. 共享fixtures
2. 编写钩子方法
3. 用于导入上级目录(项目根目录)

## 框架结构
```shell script
ApiAuto
├── README.md
├── apis
│   ├── customer.py
│   └── login.py
├── conftest.py
├── data
├── framework.jpg
├── pytest.ini
├── reports
│   └── log.txt
├── requirements.txt
├── testcases
│   ├── conftest.py
│   ├── contact
│   ├── customer
│   └── leads
└── utils
```
![](docs/framework.jpg)


## 使用方法
1. 下载项目源码
2. 安装依赖 `pip install -r requirements.txt`

## 依赖说明
Python三方包
- [requests](https://docs.python-requests.org/zh_CN/latest/) 用于发送HTTP请求
- [pymysql](https://pypi.org/project/PyMySQL/) 用于操作MySQL数据库
- [pytest](https://docs.pytest.org/en/latest/) 测试框架
- [allure-pytest](https://docs.qameta.io/allure/#_pytest) Pytest插件，用于生成美观的测试报告
- [pytest-level](https://pypi.org/project/pytest-level/) Pytest插件，为用例标记用例等级
- [pytest-base-url](https://pypi.org/project/pytest-base-url/) Pytest插件，为用例设置基础URL（方便切换环境）
- [pytest-timeout](https://pypi.org/project/pytest-timeout/) Pytest插件，为用例设置超时时间，防止卡死
- [pytest-rerunfailures](https://pypi.org/project/pytest-rerunfailures/) Pytest插件，在用例运行失败后自动重试
- [pytest-xdist](https://pypi.org/project/pytest-xdist/) Pytest插件，启动多个进程执行用例

其他工具
- [allure-commandline](https://docs.qameta.io/allure/#_installing_a_commandline): Allure命令行工具，用于将报告数据转为HTML测试报告

并发执行用例

## 参考
- [框架搭建参考1-Pytest实战API测试框架](https://www.jianshu.com/p/40a0b396465c)
- [框架搭建参考2-Pytest实战UI测试框架](https://www.jianshu.com/p/9a03984612c1)
- [pytest钩子方法](https://www.cnblogs.com/superhin/p/11733499.html)
- [pytest.ini配置项](https://www.cnblogs.com/superhin/p/11733571.html)

## 已知问题
- [x] 调试时日志文件生成不到根目录的reports下
- [ ] pytest.ini中的敏感数据处理
- [ ] 压缩报告并发送
