"""编写全局的fixtures方法"""
import os
import pytest
import yaml
import pymysql
import requests

from apis.customer import Customer


@pytest.fixture(scope='session')
def user(env_vars):
    """从pytest.ini对应环境段中读取user配置并解析成username, password"""
    user = env_vars.get('user', '')
    if not user or ',' not in user:
        pytest.skip('pytest.ini对应环境段未配置user或格式不正确, 格式应为user=CRM用户名,CRM密码')
    username, password = user.split(',', 1)
    username, password = username.strip(), password.strip()
    return username, password


@pytest.fixture(scope='session')
def customer(base_url, user):
    username, password = user
    return Customer(username, password, base_url)


# todo remove
@pytest.fixture(scope='session')
def data(request):  # request是pytest一个内置的fixture方法，里面包含pytest很多配置，调用等各种信息
    root_dir = request.config.rootdir
    data_file_path = os.path.join(root_dir, 'data', 'data.yaml')
    with open(data_file_path, encoding='utf-8') as f:
        data_dict = yaml.safe_load(f)  # yaml文件转字典
        return data_dict

# todo remove
@pytest.fixture(scope='session')
def cur():
    con = pymysql.connect(
        host='115.28.108.130',
        port=3306,
        user='test',
        password='abc123456',
        db='longtengserver',
        autocommit=True,
        charset='utf8'
    )
    cur = con.cursor(pymysql.cursors.DictCursor)
    yield cur
    cur.close()
    con.close()

# todo remove
@pytest.fixture(scope='session')
def login():
    username, password = '18010181267', 'HANzhichao@123'
    url = 'https://www.72crm.com/api/login'
    json_data = {
        'username': username,
        'password': password
    }
    res = requests.post(url, json=json_data)
    res_dict = res.json()  # 响应对象的方法，只有当响应是JSON格式才能用
    admin_token = res_dict['data']['adminToken']
    return admin_token


# todo remove
@pytest.fixture(scope='session')
def session(login):
    s = requests.session()
    s.headers['Admin-Token'] = login
    return s
