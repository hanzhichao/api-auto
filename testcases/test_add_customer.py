"""测试添加客户"""
import os

import pytest
from allure import feature, story

from utils.data import data
from utils.deco import mark

DATA = data.load_json('customers.json')


@mark(feature='添加客户', story='仅客户名称', severity='critical', marks=['smoke', 'jw'])
def test_customer_01(customer):
    """只有customerName，其他都不传"""
    entity = {'customerName': '客户01'}
    res_dict = customer.add(entity)
    assert res_dict['code'] == 0
    customer.del_by_name('客户01')


@feature('添加客户')
@story('正常添加客户')
@pytest.mark.hzc
@pytest.mark.ddt
@pytest.mark.parametrize('entity', DATA.values(), ids=DATA.keys())
def test_customer_02(customer, entity):
    """数据驱动测试"""
    res_dict = customer.add(entity)
    assert res_dict['code'] == 0
    name = entity.get('customerName')
    if name:
        customer.del_by_name(name)


