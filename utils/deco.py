'''
@file: deco.py
@author: superhin
@date : 2021/9/16
'''
import allure
import pytest


def mark(epic=None, feature=None, story=None, severity=None, marks=None):
    def _mark(func):
        if epic:
            func = allure.epic(epic)(func)
        if feature:
            func = allure.feature(feature)(func)
        if story:
            func = allure.story(story)(func)
        if severity:
            func = allure.severity(severity)(func)

        if marks:
            for _mark in marks:
                func = pytest.mark.__getattr__(_mark)(func)
            return func
    return _mark
