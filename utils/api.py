"""
1. 处理授权（业务类实现）
2. 添加请求参数和响应日志
3. 支持切换base_url
"""
import json
import logging

import requests


class HTTP:
    def __init__(self, base_url=None):
        self.base_url = base_url
        self.session = requests.session()

    def request(self, method, url, **kwargs):
        if self.base_url and not url.startswith('http'):  # 实现拼接url
            url = self.base_url + url
        try:
            res = self.session.request(method, url, **kwargs)
            logging.info('请求 %s' % url)  # todo
            logging.info('参数 %s' % json.dumps(kwargs, ensure_ascii=False))  # todo
            logging.info('响应 %s' % res.text)  # todo
            res_dict = res.json()
            # assert res_dict['code'] == 0
        except requests.exceptions.SSLError as ex:
            logging.error('SLL握手异常: %s ' % ex)
            raise
        except requests.exceptions.ConnectionError as ex:
            logging.error('建立连接异常: %s ' % ex)
            raise
        except requests.exceptions.ReadTimeout as ex:
            logging.error('读取响应超时: %s ' % ex)
            raise
        except json.decoder.JSONDecodeError:
            logging.error('响应不是JSON格式: %s' % res.text)
            raise
        else:
            return res_dict

    def get(self, url, **kwargs):
        return self.request('GET', url, **kwargs)

    def post(self, url, **kwargs):
        return self.request('POST', url, **kwargs)


if __name__ == '__main__':
    http = HTTP(base_url='https://httpbin.org')
    res_dict = http.get('/get', params={'a': 1})
    print(res_dict)
