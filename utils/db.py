'''
@file: db.py
@author: superhin
@date : 2021/9/12
'''
"""MySQL数据库操作"""
import logging

import pymysql


class DB:
    def __init__(self, host, user, password,
                 db, port=3306, charset='utf8'):
        logging.info('连接数据库 %s' % host)
        self.conn = pymysql.connect(host=host,
                                    user=user,
                                    password=password,
                                    port=port,
                                    db=db,
                                    charset=charset,
                                    autocommit=True)
        self.cur = self.conn.cursor(pymysql.cursors.DictCursor)

    def query(self, sql):
        """执行查询sql并返回所有查询结果List(Dict)"""
        self.cur.execute(sql)
        result = self.cur.fetchall()
        logging.info('查询SQL：%s 结果 %s' % (sql, result))
        return result

    def change(self, sql):
        """执行修改sql并返回影响行数"""
        result = self.cur.execute(sql)
        logging.info('执行修改SQL：%s 影响行数 %s' % (sql, result))
        return result

    def close(self):
        logging.info('断开数据库连接')
        self.cur.close()
        self.conn.close()


if __name__ == '__main__':
    from pprint import pprint
    db = DB('115.28.108.130','test', 'abc123456', 'apitest')
    print(db.change('update user set passwd=888 where name="李六";'))
    pprint(db.query('SELECT * FROM user where name="李六";'))
    db.close()
